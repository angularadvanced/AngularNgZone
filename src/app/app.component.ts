import { Component, NgZone } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'AnularNgZone';
  cpt!: number;

  constructor(private myzone: NgZone,
              private httpClient: HttpClient) {
    this.myzone.onError.subscribe((e) => {
      console.log('Erreur détecté dans la zone ' + JSON.stringify(e));
    });
  }

  incInZone() {
    this.cpt = 0;
    this.incCptInZone();
  }

  incCptInZone() {
    this.cpt++;
    console.log('cpt:${this.cpt}');
    if (this.cpt < 100) {
      setTimeout(() => {
        this.incCptInZone();
      }, 10);
    } else {
      // console.log('end cpt');
      this.myzone.run(() => {
        console.log('end cpt');
      });
    }
  }

  inOutZone() {
    this.cpt = 0;
    this.incptOutZone();
  }

  incptOutZone() {
    this.cpt = 0;
    this.myzone.runOutsideAngular(() => {
      this.incCptInZone();
    });
  }

  callApi() {
    this.httpClient.get('http://localhost:8080/api/test').subscribe();
  }
}
